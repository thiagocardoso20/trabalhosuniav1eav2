package Eleicao;

import java.util.Scanner;

public class App {

	static Candidato candidatos[] = new Candidato[3];
	static Scanner entrada = new Scanner(System.in);
	
	public static void main(String[] args) {
		int op;
		do {
			System.out.println("MENU PRINCIPAL");
			System.out.println("TECLE 1 PARA CADASTRAR");
			System.out.println("TECLE 2 PARA VOTAR");
			System.out.println("TECLE 3 PARA APURAR");
			System.out.println("TECLE 4 PARA SAIR");
			op = entrada.nextInt();
			switch (op) {
			case 1 : incluir(); break;
			case 2 : votar(); break;
			case 3 : apurar(); break;
			case 4 : break;
			}
		} while(op!=4);
	}
	
	public static void incluir(){
		for(int i = 0; i < 3; i++) {
		System.out.println("Digite o c�digo: ");
		int cod = entrada.nextInt();
		System.out.println("Digite o nome do canditado: ");
		String nome = entrada.next();
		candidatos [i] = new Candidato(cod, nome);
		}
	}

	public static void votar() {
		System.out.println("Digite o c�digo do canditado: ");
		int cod = entrada.nextInt();
		for(int i = 0; i < 3; i++) {
			if(candidatos[i].getNumero() == cod) {
				candidatos[i].incrementarVoto();
			}
		}
	}

	public static void apurar() {
		int total_votos = 0;
		for (int i = 0; i < 3; i++) {
			total_votos += candidatos[i].getVotos();
		}
		for (int i = 0; i <3; i++) {
			System.out.println(candidatos[i].getNome()
					+"---"+
					(candidatos[i].getVotos() * 100) / total_votos + "%");
		}
	}
}
