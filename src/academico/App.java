package academico;

import java.util.Scanner;

public class App {
	static List<aluno> lista = new ArrayList<Aluno>();
	
	static Scanner entrada = new Scanner(System.in);
	
	public static void main(String[] args) {
		int op;
		do {
			System.out.println("MENU PRINCIPAL");
			System.out.println("TECLE 1 PARA INSERIR ALUNO");
			System.out.println("TECLE 2 PARA EXCLUIR ALUNO");
			System.out.println("TECLE 3 PARA LAN�AR OU ALTERAR NOTAS");
			System.out.println("TECLE 4 PARA LISTAR TODOS OS ALUNOS");
			System.out.println("TECLE 5 PARA CONSULTAR SITUA��O DO ALUNO");
			System.out.println("TECLE 6 PARA SAIR");
			op = entrada.nextInt();
			switch(op) {
			case 1 : inserirAluno(); break;
			case 2 : excluirAluno(); break;
			case 3 : alterarNota(); break;
			case 4 : listarAlunos(); break;
			case 5 : consultarAluno(); break;
			case 6 : break;
			}
		}while (op != 6);
	}
	


	public static void inserirAluno() {
		System.out.println("Digite a matricula");
		int mat = entrada.nextInt();
		System.out.println("Digite o nome: ");
		String nome = entrada.next();
		lista.add(new Aluno(mat, nome));
	}
	
	public static void excluirAluno() {
		System.out.println("Digite a matricula");
		int mat = entrada.nextInt();
		for (Aluno l : lista) {
			if(l.getMatricula() == mat) {
				lista.remove(l);
				break;
			}
		}
	}
	
	public static void alterarNota() {
		System.out.println("Digite a matricula: ");
		int mat = entrada.nextInt();
		for(Aluno lst : lista) {
			if(lst.getMatricula() == mat) {
				System.out.println("Digite a AV1: ");
				lst.setAv1(entrada.nextDouble());
				System.out.println("Digite a AV2");
				lst.setAv2(entrada.nextDouble());
				break;
			}
		}
	}
	
	public static void listarAlunos() {
		if(lista.isEmpty()==false) {
			System.out.println("Lista de alunos cadastrados: ");
			for(Aluno lst : lista) {
				lst.saida();
			}
		}
		else { 
			System.out.println("Lista vazia! N�o ha aluno cadastrado");
		 }
		}
	
	
	public static void consultarAluno() {
		System.out.println("Digite a matricula do aluno: ");
		int m = entrada.nextInt();
		for (Aluno lst : lista)
		{
			if(lst.getMatricula() == m)
			{
				double media = (lst.getAv1() + lst.getAv2()) / 2;
				if (media >= 6.0)
				{
					System.out.println("AV1" + lst.getAv1());
					System.out.println("AV2" + lst.getAv2());
					System.out.println("M�dia" + media);
					System.out.println("Aprovado!");
				}
				else if (media < 6.0)
				{
					System.out.println("AV1: " + lst.getAv1());
					System.out.println("AV2: " + lst.getAv2());
					System.out.println("M�dia: " + media);
					System.out.println("Reprovado!");
				}
			}
		}
	}
}