package bancosimples;

import java.util.Scanner;

public class Aplicativo {

	static Banco banco = new Banco();
	
	static Scanner entrada = new Scanner (System.in);
	
	public static void main(String[] args) {
		
		int op;
		do {
			System.out.println("MENU PRINCIPAL");
			System.out.println("1 PARA CADASTRAR CONTA");
			System.out.println("2 PARA EXCLUIR CONTA");
			System.out.println("3 PARA VER O SALDO DA CONTA");
			System.out.println("4 PARA DEBITAR SALDO DA CONTA");
			System.out.println("5 PARA CREDITAR SALDO DA CONTA");
			System.out.println("6 PARA TRANSFERIR");
			System.out.println("7 PARA IMPRIMIR O SALDO DAS CONTAS");
			System.out.println("8 PARA SAIR");
			op = entrada.nextInt();
			switch(op) {
			case 1 : incluirConta(); break;
			case 2 : excluirConta(); break;
			case 3 : imprimirSaldoDaConta(); break;
			case 4 : debitar(); break;
			case 5 : creditar(); break;
			case 6 : transferir(); break;
			case 7 : imprimirSaldoDasContas(); break;
			case 8 : break;
			}
		} while (op!=8);
	}
	
	public static void incluirConta() {
		System.out.println("Digite o n�mero da conta: ");
		long num = entrada.nextLong();
		System.out.println("Digite o saldo da conta: ");
		double saldo = entrada.nextDouble();
		
		banco.cadastrar(new Conta(num, saldo));
		
	}
	
	public static void excluirConta() {
		System.out.println("Digite o numero da conta: ");
		long num = entrada.nextLong();
		banco.excluir(num);
		
	}
	
	public static void imprimirSaldoDaConta() {
		System.out.println("Digite o numero da conta: ");
		long num = entrada.nextLong();
		banco.printSaldo(num);
	}
	
	public static void debitar() {
		System.out.println("Digite o n�mero da conta: ");
		long num = entrada.nextLong();
		System.out.println("Digite o valor do d�bito: ");
		double valor = entrada.nextDouble();
		banco.debitar(num, valor);
	}
	
	public static void creditar() {
		System.out.println("Digite o n�mero da conta: ");
		long num = entrada.nextLong();
		System.out.println("Digite o valor a ser creditado: ");
		double valor = entrada.nextDouble();
		banco.creditar(num, valor);
		
	}
	
	public static void transferir() {
		System.out.println("Digite o numero da conta de origem:  ");
		long numOrigem = entrada.nextLong();
		System.out.println("Digite o numero da conta de destino");
		long numDestino = entrada.nextLong();
		System.out.println("Digite o valor da transfer�ncia:  ");
        double valor = entrada.nextDouble();
        banco.transferir(numOrigem, numDestino, valor);
        
	}
	
	public static void imprimirSaldoDasContas() {
		banco.getSaldoTotal();
	}
	
}
